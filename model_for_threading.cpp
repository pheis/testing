#include<iostream>
#include<mutex>
#include<stdlib.h>
#include<thread>
#include <unistd.h>


std::mutex keys_logics;
std::mutex logics_graphics;

int GLOBAL_KEY = 12;

int GLOBAL_STATE = 34;

void keys() {
        bool more_keys = true;
        int pressed_key = 1234;

        while(true) {
          if(more_keys) {
            more_keys=false;
            usleep(5 * 1000);
            pressed_key = rand() % 100;
          }
          keys_logics.lock();

          GLOBAL_KEY = pressed_key;
          more_keys = true;

          keys_logics.unlock();
        }
}

void logics() {
  int internalState = 0;
  int the_key_press = 0;
  while(true) {

    keys_logics.lock();
    the_key_press = GLOBAL_KEY;
    keys_logics.unlock();

    internalState += the_key_press;

    logics_graphics.lock();
    GLOBAL_STATE = internalState;
    logics_graphics.unlock();


  }

}

void graphics() {
  while(true) {

    logics_graphics.lock();
    int internalState = GLOBAL_STATE;
    logics_graphics.unlock();

    std::cout << internalState << std::endl;

  }
}



int main() {
  std::thread t1(&keys);
  std::thread t2(&logics);
  std::thread t3(&graphics);

  t1.join();
  t2.join();
  t3.join();

  return 0;
}


